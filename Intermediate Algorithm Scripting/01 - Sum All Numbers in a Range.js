/*
We'll pass you an array of two numbers. Return the sum of those two numbers and all numbers between them.

The lowest number will not always come first.
 */

function sumAll(arr) {

  var max;
  var min;
  var total;

  max = Math.max(arr[0], arr[1]);
  min = Math.min(arr[0], arr[1]);

  for (var i = min + 1; i <= max; i++) {
    total = min += i;
  }

  return total;

}

sumAll([5, 10]); // 45
