//Variables

var colors = [
	/*TURQUOISE*/
	"#1abc9c",
	/*EMERALD*/
	"#2ecc71",
	/*PETER RIVER*/
	"#3498db",
	/*AMETHYST*/
	"#9b59b6",
	/*WET ASPHALT*/
	"#34495e",
	/*GREEN SEA*/
	"#16a085",
	/*NEPHRITIS*/
	"#27ae60",
	/*BELIZE HOLE*/
	"#2980b9",
	/*WISTERIA*/
	"#8e44ad",
	/*MIDNIGHT BLUE*/
	"#2c3e50",
	/*SUN FLOWER*/
	"#f1c40f",
	/*CARROT*/
	"#e67e22",
	/*ALZARIN*/
	"#e74c3c",
	/*CONCRETE*/
	"#95a5a6",
	/*ORANGE*/
	"#f39c12",
	/*PUMPKIN*/
	"#d35400",
	/*POMEGRANATE*/
	"#c0392b",
	/*WISTFUL*/
	"#aea8d3",
	/*SILVER TREE*/
	"#68c3a3",
	/*SNUFF*/
	"#dcc6e0",
	/*SALEM*/
	"#1e824c",
	/*EUCALYPTUS*/
	"#26a65b",
	/*BUTTERCUP*/
	"#f39c12",
	/*FIRE BUSH*/
	"#eb9532",
	/*OLD BRICK*/
	"#96281b",
	/*PICKLED BLUEWOOD*/
	"#34495e",
	/*HOKI*/
	"#67809f",
	/*MING*/
	"#336e7b",
	/*ROYAL BLUE*/
	"#4183d7",
	/*MOUNTAIN MEADOW*/
	"#1bbc9b",
	/*PLUM*/
	"#913d88"
];

//Colors from flatuicolorpicker.com and flatuicolors.com

var quotes = [{
	"quote": "I'm commander Shepard, and this is my favorite store on the citadel.",
	"credit": "Commander Shepard",
	"game": "Mass Effect 2"
}, {
	"quote": "I run this ship, military!",
	"credit": "Commander Shepard",
	"game": "Mass Effect 2"
}, {
	"quote": "Are you still there?",
	"credit": "Turret",
	"game": "Portal"
}, {
	"quote": "Oh, hi. So, how are you holding up? BECAUSE I AM A POTATO!",
	"credit": "GLaDOS",
	"game": "Portal 2"
}, {
	"quote": "The right man in the wrong place can make all the difference in the world.",
	"credit": "The G-Man",
	"game": "Half Life 2"
}, {
	"quote": "What is a man? A miserable little pile of secrets.",
	"credit": "Dracula",
	"game": "Castlevania: Symphony of the Night"
}, {
	"quote": "Stay frosty.",
	"credit": "Captain Price",
	"game": "Call of Duty 4: Modern Warfare"
}, {
	"quote": "A man chooses, a slave obeys.",
	"credit": "Andrew Ryan",
	"game": "Bioshock"
}, {
	"quote": "I need a weapon.",
	"credit": "Master Chief",
	"game": "Halo 2"
}, {
	"quote": "Niko, it's Roman. Let's go bowling.",
	"credit": "Roman Bellic",
	"game": "Grand Theft Auto IV"
}, {
	"quote": "OBJECTION!",
	"credit": "Phoenix Wright",
	"game": "Phoenix Wright: Ace Attorney"
}, {
	"quote": "It's dangerous to go alone! Take this.",
	"credit": "Unnamed Man",
	"game": "The Legend of Zelda"
}, {
	"quote": "I am error.",
	"credit": "Villager",
	"game": "Zelda II: The Adventure of Link"
}, {
	"quote": "The Train's at home on the rails!",
	"credit": "Augustus \"Cole Train\" Cole",
	"game": "Gears of War"
}, {
	"quote": "Do a barrel roll!",
	"credit": "Peppy Hare",
	"game": "Star Fox 64"
}, {
	"quote": "The President has been kidnapped by ninjas. Are you a bad enough dude to rescue the president?",
	"credit": "Game Dialogue",
	"game": "Bad Dudes"
}, {
	"quote": "That was too close, you were almost a Jill sandwich.",
	"credit": "Barry Burton",
	"game": "Resident Evil"
}, {
	"quote": "BUY SOMETHIN' WILL YA!",
	"credit": "Shopkeeper",
	"game": "The Legend of Zelda"
}, {
	"quote": "Never pay more than 20 bucks for a computer game.",
	"credit": "Guybrush Threepwood",
	"game": "The Secret of Monkey Island"
}, {
	"quote": "All your base are belong to us.",
	"credit": "CATS",
	"game": "Zero Wing"
}, {
	"quote": "It's a me, Mario!",
	"credit": "Mario",
	"game": "Super Mario 64"
}, {
	"quote": "THANK YOU MARIO! BUT OUR PRINCESS IS IN ANOTHER CASTLE!",
	"credit": "Toad",
	"game": "Super Mario Bros."
}, {
	"quote": "Waka waka waka waka waka.",
	"credit": "Pac-Man",
	"game": "Pac-Man"
}, {
	"quote": "Hey dudes thanks, for rescuing me. Let's go for a burger.... Ha! Ha! Ha! Ha!",
	"credit": "The President",
	"game": "Bad Dudes"
}, {
	"quote": "You exist because we allow it, and you will end because we demand it.",
	"credit": "Sovereign",
	"game": "Mass Effect"
}, {
	"quote": "Mess with the best, you will die like the rest.",
	"credit": "Duke Nukem",
	"game": "Duke Nukem 3D"
}];

function quoteGenerator() {

	quoteID = quotes[Math.floor(Math.random() * quotes.length)];
	document.getElementById("quote").innerHTML = '"' + quoteID.quote + '"';
	document.getElementById("credit").innerHTML = quoteID.credit;
	document.getElementById("game").innerHTML = quoteID.game;

	colorID = colors[Math.floor(Math.random() * colors.length)];
	document.body.style.backgroundColor = colorID;

	document.getElementById("newQuoteButton").style.backgroundColor = colorID;

}

function backgroundFill(x) {
	document.getElementById(x).style.backgroundColor = colorID;
}

function removeBackgroundFill(x) {
	document.getElementById(x).style.backgroundColor = "transparent";
}

function tweetQuote() {
	window.open('https://twitter.com/intent/tweet?hashtags=VideoGameQuotes&text=' + encodeURIComponent('"' + quoteID.quote + '" ' + quoteID.credit + ", " + quoteID.game));
}