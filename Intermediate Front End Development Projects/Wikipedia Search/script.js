$(document).ready(function(){

var searchField = $('#search-field');
var searchButton = $('#search-button');

searchField.val('');
searchField.keyup(function() {
var searchTerm = searchField.val();

$.ajax({
    url: 'https://en.wikipedia.org//w/api.php',
    dataType: 'jsonp',
    data: {
        action: 'opensearch',
        limit: 8,
        'format': 'json',
        'search': searchTerm,
    },
    success: function(data) {
        buildResults(data);
    }

});

var buildResults = function(results) {

$('#search-query, #search-results').empty();
if ( searchTerm.length > 0 ) {
    $('#search-query').html('Results for "' + searchTerm + '"');

        for (var x = 0; x < results[1].length; x++ ) {
            var resultTitle = results[1][x];
            var resultDesc = results[2][x];
            var resultLink = results[3][x];

            var resultEntry = `
                <li>
                    <a href="` + resultLink + `">
                        <span class="result-title">` + resultTitle + `</span>
                        <span class="result-description">` + resultDesc + `<span>
                    </a>
                </li>
            `;

            $("#search-results").append( resultEntry );
        }

}
};//buildResults

});
});