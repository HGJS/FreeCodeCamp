$(document).ready(function(){

var api = 'c1bc2facbd8eb75b78c447431ac098f6';

//Location Data
$.getJSON("http://ip-api.com/json/?callback=?", function(locationData) {
    $.each(locationData, function(key, val) {
    });
    var lat = locationData.lat;
    var lon = locationData.lon;
    var url = 'http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&units=metric&APPID=' + api + '';

    //Weather Data
    $.getJSON(url, function(weatherData) {
        $.each(weatherData, function(key, val) {
    });
    var location = weatherData['name'];
    var main = weatherData['weather'][0]['main'];
    var icon = weatherData['weather'][0]['icon'];
    var temp =  weatherData['main']['temp'];
    var tempC = Math.round(temp);
    var tempF = Math.round((temp * 1.8) + 32);

    $('#location').text(location);
    $('#weather-type').text(main);
    $('#temperature').html(tempC + '&deg;C');
    $('#switch-temp').html('&deg;F');

//Switch Temperature
var metric = true;
$('#switch-temp').click(function(){
metric = !metric;

if ( metric ) {
    $('#temperature').html(tempC + '&deg;C');
    $('#switch-temp').html('&deg;F');
} else {
    $('#temperature').html(tempF + '&deg;F');
    $('#switch-temp').html('&deg;C');
}
});

//Icons
var weatherIcon = function(){
var x = icon;
var iconClass;
var message;

    switch(x) {
    //Clear Day
        case '01d':
            message = 'Clear Day';
            iconClass = 'icon-clear-day';
            break;
    //Clear Night
        case '0ln':
            message = 'Clear Night';
            iconClass = 'icon-clear-night';
            break;
    //Cloudy Day
        case '02d':
        case '03d':
        case '04d':
            message = 'Cloudy Day';
            iconClass = 'icon-cloudy-day';
            break;
    //Cloudy Night
        case '02n':
        case '03n':
        case '04n':
            message = 'Cloudy Night';
            iconClass = 'icon-cloudy-night';
            break;
    //Light Rain Day
        case '09d':
            message = 'Light Rain Day';
            iconClass = 'icon-light-rain-day';
            break;
    //Light Rain Night
        case '09n':
            message = 'Light Rain Night';
            iconClass = 'icon-light-rain-night';
            break;
    //Heavy Rain Day
        case '10d':
            message = 'Heavy Rain Day';
            iconClass = 'icon-heavy-rain-day';
            break;
    //Heavy Rain Night
        case '10n':
            message = 'Heavy Rain Night';
            iconClass = 'icon-heavy-rain-night';
            break;
    //Thunderstorm Day
        case '11d':
            message = 'Thunderstorms Day';
            iconClass = 'icon-thunderstorm-day';
            break;
    //Thunderstorm Night
        case '11n':
            message = 'Thunderstorms Night';
            iconClass = 'icon-thunderstorm-night';
            break;
    //Snow Day
        case '13d':
            message = 'Snow Day';
            iconClass = 'icon-snow-day';
            break;
    //Snow Night
        case '13n':
            message = 'Snow Night';
            iconClass = 'icon-snow-night';
            break;
    //Mist Day
        case '50d':
            message = 'Mist Day';
            iconClass = 'icon-mist-day';
            break;
    //Mist Night
        case '50n':
            message = 'Mist Night';
            iconClass = 'icon-mist-night';
            break;

        default:
            message = 'Some weird weather';
            iconClass = 'icon-none';
    }

    console.log(message);

$('.icon').each(function(){
    if( $(this).hasClass(iconClass) ) {
        $(this).removeClass('hidden');
        $('.icon').not( $(this) ).addClass('hidden');
    }
});

}
weatherIcon();

});

});

});