$(document).ready(function(){

var buildView;
var channels = ['freecodecamp', 'xbox', 'playstation', 'nintendo', 'bungie', 'battlefield', 'halo'];

channels.forEach(function(element){
    buildView(element);
});

function buildView(name) {

$.getJSON({
    url: 'https://wind-bow.gomix.me/twitch-api/channels/' + name + '?callback=?',
    dataType: 'json',
    success: function(results) {
        buildResults(results);
    }
});

var buildResults = function(results) {
console.log(results);
            var resultClass = 'online';
            var onlineStatus = 'Online';
            var resultLogo = results.logo;
            var resultTitle = results.display_name;
            var resultStatus = results.status;
            var resultStream = results.game;
                if ( resultStream  == null ) {
                    resultStream = 'Offline';
                    resultClass = "offline";
                    onlineStatus = 'Offline';
                }
            var resultLink = results.url;

            var resultEntry = `
                <li class="` + resultClass + `">
                        <span class="result-logo"><a href="` + resultLink + `" target="_blank"><img src="` + resultLogo + `"></a></span>
                            <div class="result-details">
                                <span class="result-title"><a href="` + resultLink + `" target="_blank">` + resultTitle + `</a></span>
                                <span class="result-status">` + resultStatus + `</span>
                                <span class="result-stream">` + resultStream + `</span>
                            </div>
                        <span class="online-status">` + onlineStatus + ` <span class="online-status-icon"></span>
                </li>
            `;

            if ( resultClass == 'online' ) {
                $("#streams-list").prepend( resultEntry );
            } else {
                $("#streams-list").append( resultEntry );
            }

};//buildResults

}//buildView

$('#online').click(function(){
    $('li').each(function(){
    var el = $(this);
        if ( !el.hasClass('online') ) {
            el.addClass('hide');
        } if ( el.hasClass('online') && el.hasClass('hide') ) {
            el.removeClass('hide');
        }
    });
});

$('#offline').click(function(){
    $('li').each(function(){
    var el = $(this);
        if ( !el.hasClass('offline') ) {
            el.addClass('hide');
        } if ( el.hasClass('offline') && el.hasClass('hide') ) {
            el.removeClass('hide');
        }
    });
});

$('#all').click(function(){
    $('li').each(function(){
        var el = $(this);
        if ( el.hasClass('hide') ) {
            el.removeClass('hide');
        }
    });
});

});