/*
Write a function that splits an array (first argument) into groups the length of size (second argument) and returns them as a two-dimensional array.
 */

function chunkArrayInGroups(arr, size) {

 arr = arr.slice();
  var arr2 = [];
  for(var i = 0, l = arr.length; i < l; i+=size) {
    arr2.push(arr.slice(0, size));
    arr = arr.slice(size);
  }
  return arr2;
}

chunkArrayInGroups(["a", "b", "c", "d", "e", "f"], 2);
