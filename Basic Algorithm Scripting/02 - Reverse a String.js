/*
Reverse the provided string.

You may need to turn the string into an array before you can reverse it.

Your result must be a string.
*/


function reverseString(str) {

return str.split("").reverse().join("");

}

reverseString("Jenny has some beautiful trucks. Those lawyers don't own a single hammer. He has a fork.");
