/*
Return the provided string with the first letter of each word capitalized. Make sure the rest of the word is in lower case.

For the purpose of this exercise, you should also capitalize connecting words like "the" and "of".
 */

function titleCase(str) {

  var array = [];

  array = str.toLowerCase().split(" ");

  for(i = 0; i < array.length; i ++) {
        array[i] = array[i].charAt(0).toUpperCase() + array[i].substr(1);
  }
      return array.join(" ");
}

titleCase("I'm a little tea pot");
