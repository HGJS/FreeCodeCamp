/*
Return the length of the longest word in the provided sentence.

Your response should be a number.
*/

function findLongestWord(str) {

  var words = [];

  words = str.split(" ");

  var lengths = words.map(function(value){

  return value;

  });

  lengths.sort(function(a, b){
    return b.length - a.length;

  });

  return lengths[0].length;

}

findLongestWord("The quick brown fox jumped over the lazy dog");