/*
Return the factorial of the provided integer.

If the integer is represented with the letter n, a factorial is the product of all positive integers less than or equal to n.

Factorials are often represented with the shorthand notation n!

For example: 5! = 1 * 2 * 3 * 4 * 5 = 120
*/

function factorialize(num) {

  var numArray = [];

  if(num === 0) {
    return 1;
  }
  else {

    for(val = num; val > 0; val -= 1) {
       numArray.push(val);
    }

      }

  var factorial = numArray.reduce(function(previousVal, currentVal) {
    return previousVal * currentVal;

  });

  return factorial;

}

factorialize(20);
