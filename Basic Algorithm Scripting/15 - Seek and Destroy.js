/*
You will be provided with an initial array (the first argument in the destroyer function), followed by one or more arguments. Remove all elements from the initial array that are of the same value as these arguments.
 */

function destroyer(arr) {
  // Remove all the values

  var args = Array.from(arguments);

 function search (value) {
   return args.indexOf(value) === -1;
 }

  return arr.filter(search);
}

destroyer(["tree", "hamburger", 53], "tree", 53);